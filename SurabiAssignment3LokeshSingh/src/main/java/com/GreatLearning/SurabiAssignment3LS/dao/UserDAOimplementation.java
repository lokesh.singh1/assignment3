package com.GreatLearning.SurabiAssignment3LS.dao;
import com.GreatLearning.SurabiAssignment3LS.controller.Order;
import com.GreatLearning.SurabiAssignment3LS.entity.Admin;
import com.GreatLearning.SurabiAssignment3LS.entity.Bills;
import com.GreatLearning.SurabiAssignment3LS.entity.Customer;
import com.GreatLearning.SurabiAssignment3LS.entity.FoodMenu;

import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

//Implementation of all the functionalities of userDAO class (Definations of methods)
@Repository
public class UserDAOimplementation implements UserDAO {
	
	//Dependency Injection
	@Autowired
	private EntityManager entityManager;
	//Keeping a global HashMap containing KEY:Value as ItemNo : Price from foodMenu table.
	private HashMap<Integer,Integer> map;
	//Keeping a global variable to store the username of current User of our application.
	private String username;
	
	@Override
	public boolean login(Customer theUser) {
		Session currentSession = entityManager.unwrap(Session.class);
		Customer customer = currentSession.get(Customer.class,theUser.getId());
		if(customer!=null && theUser.getName().equals(customer.getName()) && theUser.getPassword().equals(customer.getPassword())) {
			return true ;
		}
		return false;
	}

	@Override
	public String logout() {
		map = null ;
		username = null ;
		return "User Successfully Logged Out\n" ;
	}

	@Override
	public List<FoodMenu> getFoodList(Customer theUser) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query theQuery = currentSession.createQuery("from FoodMenu", FoodMenu.class);
		List<FoodMenu> foodmenus = theQuery.getResultList();
		map = new HashMap<>();
		username = theUser.getName();
		for(int i=0 ; i<foodmenus.size(); i++) {
			map.put(foodmenus.get(i).getId(), foodmenus.get(i).getPrice());
		}
		return foodmenus;
	}

	@Override
	public boolean login(Admin theUser) {
		Session currentSession = entityManager.unwrap(Session.class);
		Admin admin = currentSession.get(Admin.class,theUser.getId());
		if (admin == null)return false ;
		return (theUser.getName().equals(admin.getName()) && theUser.getPassword().equals(admin.getPassword()));
	}

	@Override
	public String showTotalSaleForMonth() {
		Session currentSession = entityManager.unwrap(Session.class);
		int month=java.time.LocalDate.now().getMonthValue();
		String hql = "from Bills where MONTH(date) = "+month+"";
		Query query = currentSession.createQuery(hql);
		List<Bills> results = query.list();
		if(results.size() == 0) return "No sale for this month" ;
		int sale = 0;
		for(int i=0 ; i<results.size() ; i++) {
			sale += results.get(i).getBill();
		}
		return String.valueOf(sale);
	}

	@Override
	public List<Bills> showAllBillsForToday() {
		Session currentSession = entityManager.unwrap(Session.class);
		int day= java.time.LocalDate.now().getDayOfMonth();
		String hql = "from Bills where DAY(date) = "+day+"";
		Query query = currentSession.createQuery(hql);
		List<Bills> results = query.list();
		return results ;
	}

	@Override
	public String takeOrder(int order) {
		Integer bill = order ;
		String date = java.time.LocalDate.now().toString();
		Bills bills = new Bills();
		bills.setName(username);
		bills.setDate(date);
		bills.setBill(bill);
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.save(bills);
		return "Thank you for ordering with us!"
				+ "\nYour total bill is = " + bill +"" ;
	}
	@Override
	public String registerCustomer(Customer customer) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.save(customer);
		return "User successfuly registered with surabi as Customer" ;
	}

	@Override
	public String deleteCustomer(int theid) {
		System.out.println("inside del");
		Session currentSession = entityManager.unwrap(Session.class);
		Query theQuery = currentSession.createQuery("delete from users where id = :xyz");
		theQuery.setParameter("xyz", theid);
		theQuery.executeUpdate();
		return "the user data with id ="+theid+" is deleted";
	}

	@Override
	public Customer findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class);
		Customer  customer = currentSession.get(Customer.class,theId);
		return customer;
	}

}

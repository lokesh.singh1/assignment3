package com.GreatLearning.SurabiAssignment3LS.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.GreatLearning.SurabiAssignment3LS.controller.Order;
import com.GreatLearning.SurabiAssignment3LS.entity.Admin;
import com.GreatLearning.SurabiAssignment3LS.entity.Bills;
import com.GreatLearning.SurabiAssignment3LS.entity.Customer;
import com.GreatLearning.SurabiAssignment3LS.entity.FoodMenu;

//Class having all the functionalities that our application is having.
public interface UserDAO {
	
	public boolean login(Customer theUser) ;
	public boolean login(Admin theUser) ;
	public String logout() ;
	public List<FoodMenu> getFoodList(Customer theUser) ;
	public String showTotalSaleForMonth() ;
	public List<Bills> showAllBillsForToday() ;
	public String takeOrder(int order) ;
	public String registerCustomer(Customer customer);
	public String deleteCustomer(int id);
	public Customer findById(int theId);
	
}

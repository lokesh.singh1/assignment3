package com.GreatLearning.SurabiAssignment3LS.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.GreatLearning.SurabiAssignment3LS.entity.Customer;
import com.GreatLearning.SurabiAssignment3LS.entity.FoodMenu;
import com.GreatLearning.SurabiAssignment3LS.service.UserService;

//Mapping the Api endPoints for all the Customer Functionality of our application.
@RestController
@RequestMapping("/surabi/customer")
@CrossOrigin(origins = "http://localhost:3000")
public class CustomerController {
	@Autowired
	private UserService user;

	//Keeping global variable to track if Customer has logged in first or not!
	private boolean validCustomer = false;
	
	//Checking credentials of customer and if true then showing the foodMenu.
	@PostMapping("/login")
	public List<FoodMenu> getFoodMenu(@RequestBody Customer customer) {
		validCustomer = user.login(customer);
		if (!validCustomer) {
			System.out.println("Wrong credentials, try again");
			return null;
		}
		return user.getFoodList(customer);
	}
	
	@GetMapping("/logout")
	public String logout(){
		if(!validCustomer) return "No one has logged in yet, please log in!\n" ;
		validCustomer = false ;
		return user.logout();
	}
	
	@GetMapping("/order/{order}")
	public String takeOrder(@PathVariable int order) {
		if(validCustomer) return user.takeOrder(order);
		return "Login first\n" ;
	}
	
	@PostMapping("/register")
	public String register(@RequestBody Customer customer) {
		return user.registerCustomer(customer);
	}
}

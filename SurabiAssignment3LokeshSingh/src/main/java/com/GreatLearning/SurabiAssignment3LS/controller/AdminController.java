package com.GreatLearning.SurabiAssignment3LS.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.GreatLearning.SurabiAssignment3LS.entity.Admin;
import com.GreatLearning.SurabiAssignment3LS.entity.Bills;
import com.GreatLearning.SurabiAssignment3LS.entity.Customer;
import com.GreatLearning.SurabiAssignment3LS.service.UserService;

//Mapping the Api endPoints for all the Admin Functionality of our application.
@RestController
@RequestMapping("/surabi/admin")
@CrossOrigin(origins = "http://localhost:3000")
public class AdminController {
	
	//Dependency Injection 
	@Autowired
	private UserService userAdmin;
	
	//Keeping global variable to track if admin has logged in first or not!
	private static boolean validAdmin = false;
	
	@PostMapping("/login")
	public String adminLogin(@RequestBody Admin admin) {
		if(validAdmin) return "One admin is already logged in please wait" ;
		validAdmin = userAdmin.login(admin);
		if (validAdmin)
			return "Successfull Login, Welcome to Administraion "
					+ "\nUse @Get admin/1 to show Total Sale for this month and"
					+ "\nUse @Get admin/2 to show All Bills For today\n"
					+ "Usen @Get admin/customer to show CURD menu\n";
		return "Wrong credentials, please try again\n";
	}

	@GetMapping("/logout")
	public String logout() {
		if(!validAdmin) return "No one has logged in yet, please log In!\n" ;
		validAdmin = false ;
		return userAdmin.logout();
	}
	
	@GetMapping("/1")
	public String showTotalSaleForMonth() {
		if (validAdmin) return userAdmin.showTotalSaleForMonth();	
		return "Login first\n" ;
	}

	@GetMapping("/2")
	public List<Bills> showAllBillsForToday() {
		if (validAdmin) return userAdmin.showAllBillsForToday();
		return null;
	}
	
	@GetMapping("/customer")
	public String allOptions() {
		return "choose @PUT admin/customer to register a customer\n"
				+ "choose @Delete admin/customer to delete a customer\n"
				+ "choose @Patch admin/customer to update a Customer\n"
				+ "choose @Get admin/customer/{int id} to get customer record by id\n" ;
	}
	
	// #AdminCurd Operations on Users(Customers)
	@PutMapping("/customer")
	public String registerCustomer(@RequestBody Customer customer) {
		if (validAdmin) return userAdmin.registerCustomer(customer);
		return "Login first\n" ;
	}
	
	@DeleteMapping("/customer")
	public String deleteCustomer(@RequestBody Customer customer) {
		if (validAdmin) return userAdmin.deleteCustomer(customer);
		return "Login first\n" ;
	}
	@PatchMapping("/customer")
	public String updateCustomer(@RequestBody Customer customer) {
		if (validAdmin) return userAdmin.updateCustomer(customer);
		return "Login first\n" ;
	}
	@GetMapping("/customer/{id}")
	public Customer getCustomerById(@PathVariable int id) {
		if (validAdmin) return userAdmin.findById(id);
		return null ;
	}

}

package com.GreatLearning.SurabiAssignment3LS.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.GreatLearning.SurabiAssignment3LS.controller.Order;
import com.GreatLearning.SurabiAssignment3LS.entity.Admin;
import com.GreatLearning.SurabiAssignment3LS.entity.Bills;
import com.GreatLearning.SurabiAssignment3LS.entity.Customer;
import com.GreatLearning.SurabiAssignment3LS.entity.FoodMenu;

//It's basically to provide abstraction between the different controllers from the actual implementation/
//definations of all the methods.
@Component
public interface UserService {
	
	public boolean login(Customer theUser) ;
	public boolean login(Admin theUser) ;
	public String logout() ;
	public List<FoodMenu> getFoodList(Customer theUser) ;
	public String showTotalSaleForMonth() ;
	public List<Bills> showAllBillsForToday() ;
	public String takeOrder(int order) ;
	
	// #Admin CURD operations.
	public String registerCustomer(Customer customer) ;
	public String deleteCustomer(Customer customer);
	public String updateCustomer(Customer customer);
	public Customer findById(int theId);
}

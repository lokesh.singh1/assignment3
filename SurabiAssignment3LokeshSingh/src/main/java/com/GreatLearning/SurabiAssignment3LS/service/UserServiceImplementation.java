package com.GreatLearning.SurabiAssignment3LS.service;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.GreatLearning.SurabiAssignment3LS.controller.Order;
import com.GreatLearning.SurabiAssignment3LS.dao.AdminCURD;
import com.GreatLearning.SurabiAssignment3LS.dao.UserDAO;
import com.GreatLearning.SurabiAssignment3LS.entity.Admin;
import com.GreatLearning.SurabiAssignment3LS.entity.Bills;
import com.GreatLearning.SurabiAssignment3LS.entity.Customer;
import com.GreatLearning.SurabiAssignment3LS.entity.FoodMenu;

//Calling all the functions/methods from the Dao package to provide definations of the methods 
//required by different controllers.
@Service
public class UserServiceImplementation implements UserService{
	
	//Dependecy Injection.
	@Autowired
	private UserDAO user ;
	@Autowired
	private AdminCURD admincurd;
	
	public UserServiceImplementation() {
		
	}
	
	@Override
	public String logout() {
		// TODO Auto-generated method stub
		return user.logout();
	}

	@Override
	public List<FoodMenu> getFoodList(Customer theUser) {
		return user.getFoodList(theUser);
	}

	@Override
	public boolean login(Customer theUser) {
		return user.login(theUser);
	}

	@Override
	public boolean login(Admin theUser) {
		return user.login(theUser);
	}

	@Override
	public String showTotalSaleForMonth() {
		return user.showTotalSaleForMonth();
	}

	@Override
	public List<Bills> showAllBillsForToday() {
		return user.showAllBillsForToday();
	}

	@Override
	public String takeOrder(int order) {
		return user.takeOrder(order);
	}

	@Override
	public String registerCustomer(Customer customer) {
		return user.registerCustomer(customer);
	}

	@Override
	public String deleteCustomer(Customer customer) {
		admincurd.delete(customer);;
		return "the user data with customerid ="+customer.getId()+" is deleted";
	}

	@Override
	public String updateCustomer(Customer customer) {
		admincurd.save(customer);
		return "customer updated with id = "+customer.getId()+" with new name = "+customer.getName()+""
				+ " and password = "+customer.getPassword()+"\n" ; 
	}

	@Override
	public Customer findById(int theId) {
		return user.findById(theId);
	}

}
